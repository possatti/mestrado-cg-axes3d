#include <GL/glut.h>
#include <iostream>
#include <cmath>

using namespace std;

#define paddleHeight 80
#define paddleWidth 10
#define baseHeight 40
#define baseWidth 100
#define radiusWheel 30

struct RGB {
	float red, green, blue;
};

RGB BLACK = {0,0,0};
RGB WHITE = {1,1,1};

int prevMouseX, prevMouseY;
float horizontalRotation, verticalRotation;

bool keystate[256];
int windowSize = 700;

void drawRectangle(float width, float height, RGB color=WHITE) {
	glColor3d(color.red, color.green, color.blue);
	glBegin(GL_QUADS);
        glVertex2f(-width/2, 0);
        glVertex2f(-width/2, height);
        glVertex2f(width/2, height);
        glVertex2f(width/2, 0);
    glEnd();
}

void drawCircle(/*float cx, float cy,*/ float radius, RGB color=WHITE, int segments=32) {
	float cx=0, cy=0;
	glColor3d(color.red, color.green, color.blue);
	glBegin(GL_TRIANGLE_FAN);
	glVertex2f(cx, cy);
	for(int i = 0; i <= segments;i++) {
		glVertex2f(
			cx + (radius * cos(i * 2*M_PI / segments)),
			cy + (radius * sin(i * 2*M_PI / segments))
		);
	}
	glEnd();
}

void drawHollowCircle(/*float cx, float cy,*/ float radius, RGB color=WHITE, int segments=32) {
	float cx=0, cy=0;
	glColor3d(color.red, color.green, color.blue);
	glPointSize(3);
	glBegin(GL_POINTS);
	glVertex2f(cx, cy);
	for(int i = 0; i < segments; i++) {
		glVertex2f(
			cx + (radius * cos(i * 2*M_PI / segments)),
			cy + (radius * sin(i * 2*M_PI / segments))
		);
	}
	glEnd();
}

void DrawAxes(double size) {
	GLfloat mat_ambient_r[] = { 1,0,0,1 };
	GLfloat mat_ambient_g[] = { 0,1,0,1 };
	GLfloat mat_ambient_b[] = { 0,0,1,1 };
	GLfloat no_mat[] = { 0,0,0,1 };
	glMaterialfv(GL_FRONT, GL_EMISSION, no_mat);
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, no_mat);
	glMaterialfv(GL_FRONT, GL_SPECULAR, no_mat);
	glMaterialfv(GL_FRONT, GL_SHININESS, no_mat);

	// X axis.
	glPushMatrix();
		glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_ambient_r);
		// glColor3fv(mat_ambient_r);
		glScalef(size, size*0.1, size*0.1);
		glTranslatef(0.5,0,0);
		glutSolidCube(1.0);
	glPopMatrix();

	// X axis.
	glPushMatrix();
		glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_ambient_g);
		// glColor3fv(mat_ambient_g);
		glRotatef(90,0,0,1);
		glScalef(size, size*0.1, size*0.1);
		glTranslatef(0.5,0,0);
		glutSolidCube(1.0);
	glPopMatrix();

	// X axis.
	glPushMatrix();
		glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_ambient_b);
		// glColor3fv(mat_ambient_b);
		glRotatef(-90,0,1,0);
		glScalef(size, size*0.1, size*0.1);
		glTranslatef(0.5,0,0);
		glutSolidCube(1.0);
	glPopMatrix();
}

void DrawObj(double size) {
	GLfloat materialEmission[] = {0,0,0,1};
	GLfloat materialColor[] = {0,0,1,1};
	// GLfloat materialColorAMB[] = {1,1,0,1};
	GLfloat mat_specular[] = {1,1,1,1};
	GLfloat mat_shininess[] = {128};
	glMaterialfv(GL_FRONT, GL_EMISSION, materialEmission);
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialColor);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialColor);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
	// glColor3f(1,0,0);
	// glutSolidSphere(size, 100, 16);
	// glRotatef(45,0,1,0);
	glutSolidCube(size);
}

void mouse(int button, int state, int x, int y) {
	prevMouseX = x;
	prevMouseY = y;
}

void motion(int x, int y) {
	horizontalRotation += (x - prevMouseX);
	verticalRotation += (y - prevMouseY);
	// std::cout << "horizontalRotation: " << horizontalRotation << std::endl;
	// std::cout << "verticalRotation: " << verticalRotation << std::endl;
	prevMouseX = x;
	prevMouseY = y;
}

void keyboard(unsigned char key, int x, int y) {
	cout << "Keyboard: " << key << endl;
	keystate[key] = true;
}

void keyboardup(unsigned char key, int x, int y) {
	cout << "Keyboard Up: " << key << endl;
	keystate[key] = false;
}

void idle() {
	glutPostRedisplay();
}

void display() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0,2,5, 0,0,0, 0,1,0);

	GLfloat light_position[] = { 0,2,5,1 };
	// GLfloat light_position[] = { 0,5,0,1 };
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	glPushMatrix();
		glRotatef(horizontalRotation, 0,1,0);
		glRotatef(verticalRotation, 1,0,0);
		DrawAxes(1.5);
		DrawObj(1.0);
	glPopMatrix();

	glutSwapBuffers();
}

void init() {
	glClearColor(0,0,0,0);
	glShadeModel(GL_FLAT);
	// glShadeModel(GL_SMOOTH);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_DEPTH_TEST);
	glViewport(0,0, (GLsizei) windowSize, (GLsizei) windowSize);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45, (GLfloat) windowSize / (GLfloat) windowSize, 1, 15);
	// glOrtho(
	// 	-3,3,
	// 	-3*windowSize/windowSize,3*windowSize/windowSize,
	// 	1, 15);
}

int main(int argc, char** argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(windowSize, windowSize);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("3D Axes");
	init();
	glutIdleFunc(idle);
	glutDisplayFunc(display);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);
	glutKeyboardFunc(keyboard);
    glutKeyboardUpFunc(keyboardup);
	glutMainLoop();

	return 0;
}
